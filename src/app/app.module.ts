import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FlightformComponent } from './flightform/flightform.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';

import { MAT_DATE_LOCALE } from '@angular/material/core';
import { PageService } from './data/page.service';

@NgModule({
  
  declarations: [
    AppComponent,
    FlightformComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule
  ],
  providers: [PageService,{ provide: MAT_DATE_LOCALE,useValue:"th-TH"} ],
  bootstrap: [AppComponent]
})

export class AppModule { }
