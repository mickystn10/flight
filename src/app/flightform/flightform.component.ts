import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl, Validators } from '@angular/forms';
import { Flight} from '../components/flight'
import { PageService } from '../data/page.service';

@Component({
  selector: 'app-flightform',
  templateUrl: './flightform.component.html',
  styleUrls: ['./flightform.component.css'],
})

export class FlightformComponent implements OnInit {
  flightform: FormGroup;
  flight:Array<Flight>=[];
  constructor(private pageService: PageService) {
    this.flightform = new FormGroup({
      Name:new FormControl('',Validators.required),
      Origin:new FormControl('',Validators.required),
      Destination:new FormControl('',Validators.required),
      LeaveDate:new FormControl('',Validators.required),
      ArriveDate:new FormControl('',Validators.required),
      TripTypes:new FormControl('',Validators.required),
      Adults:new FormControl('',Validators.required),
      Child:new FormControl('',Validators.required),
      Infants:new FormControl('',Validators.required)
    });
    
  }
  
  ngOnInit(): void {
    
    this.getFlightpage();
  }
  getFlightpage(){
    this.flight = this.pageService.getFlight();
  }
  check(f:FormGroup):boolean{
    if(f.value.Origin.toString()!== f.value.Destination.toString()){
      console.log("test");
      return false;
    } 
    return true;
  }
  onSubmit(f:FormGroup): void {
    let form_record = new Flight(f.get('Name')?.value,f.get('Origin')?.value,f.get('Destination')?.value,
                                  f.get('TripTypes')?.value,f.get('Adults')?.value,f.get('LeaveDate')?.value,f.get('Child')?.value,
                                  f.get('Infants')?.value,f.get('ArriveDate')?.value);
    this.pageService.addFlight(form_record);
  }
}

