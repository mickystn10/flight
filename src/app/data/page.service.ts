import { Injectable } from '@angular/core';
import { Moc} from './moc';
import { Flight } from '../components/flight';

@Injectable({
  providedIn: 'root'
})
export class PageService {
  flights: Flight[] = [];
  constructor() { 
    this.flights = Moc.tfriend;
  }
  getFlight(): Flight[] {
    return this.flights;
  }
  addFlight(f:Flight): void{
    this.flights.push(f)
  }
}