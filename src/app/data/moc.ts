import { Flight} from '../components/flight'
export class Moc {
    public static tfriend: Flight[]= [
        {
            fullName: "Pimpicha Chaisiri",
            from: "Thailand",
            to: "Korea",
            type: "Return",
            adults: 2,
            departure: new Date(2022,2,6),
            children: 1,
            infants: 0,
            arrival: new Date(2022,2,15)
        },
        {
            fullName: "Prinyada Juicharoen",
            from: "Korea",
            to: "Laos",
            type: "One Way",
            adults: 2,
            departure: new Date(2022,2,6),
            children: 1,
            infants: 0,
            arrival: new Date(2022,2,6)
        },
        
    ];
}